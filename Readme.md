# Filtros em Fotos com CSS

Este projeto é uma aplicação web que permite aplicar diferentes filtros em fotos usando CSS. A interface fornece controles deslizantes para ajustar diversos efeitos visuais em uma imagem.

## Funcionalidades

- Aplicar filtros como desfoque, brilho, contraste, escala de cinza, rotação de tonalidade, inversão de cores, opacidade, saturação e sépia.
- Visualizar os valores dos filtros em tempo real.
- Resetar todos os filtros para os valores originais.

## Tecnologias Utilizadas

- HTML
- CSS
- JavaScript

## Licença

Este projeto está licenciado sob a Licença BSD 3-Clause.
