document.querySelector('.grupo').addEventListener('change', aplicarFiltroImagem);
document.querySelector('#resetBtn').addEventListener('click', resetarFiltros);
const imagem = document.querySelector('.imagem');
let filtrosImagem = '';

function aplicarFiltroImagem(event) {
    const efeito = event.target.className;
    const valor = event.target.value;
    document.querySelector(`#${efeito}_value`).innerText = valor;
    if (efeito === 'blur') {
        filtrosImagem += `${efeito}(${valor}px)`;
    } else {
        filtrosImagem += `${efeito}(${valor}%)`;
    }

    imagem.style.filter = filtrosImagem.trim();
}

function resetarFiltros() {
    const controles = document.querySelectorAll('.efeitos input[type="range"]');
    controles.forEach(controle => {
        controle.value = controle.getAttribute('value');
        const efeito = controle.className;
        document.querySelector(`#${efeito}_value`).innerText = controle.value;
    });
    filtrosImagem = '';
    imagem.style.filter = 'none';
}
